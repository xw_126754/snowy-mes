// import { axios } from '@/utils/request'
//
// /**
//  * 查询SaaS租户
//  *
//  * @author bwl
//  * @date 2022-08-11 14:58:43
//  */
// export function tenantInfoPage (parameter) {
//   return axios({
//     url: '/tenantInfo/page',
//     method: 'get',
//     params: parameter
//   })
// }
//
// /**
//  * SaaS租户列表
//  *
//  * @author bwl
//  * @date 2022-08-11 14:58:43
//  */
// export function tenantInfoList (parameter) {
//   return axios({
//     url: '/tenantInfo/list',
//     method: 'get',
//     params: parameter
//   })
// }
//
// /**
//  * 添加SaaS租户
//  *
//  * @author bwl
//  * @date 2022-08-11 14:58:43
//  */
// export function tenantInfoAdd (parameter) {
//   return axios({
//     url: '/tenantInfo/add',
//     method: 'post',
//     data: parameter
//   })
// }
//
// /**
//  * 编辑SaaS租户
//  *
//  * @author bwl
//  * @date 2022-08-11 14:58:43
//  */
// export function tenantInfoEdit (parameter) {
//   return axios({
//     url: '/tenantInfo/edit',
//     method: 'post',
//     data: parameter
//   })
// }
//
// /**
//  * 删除SaaS租户
//  *
//  * @author bwl
//  * @date 2022-08-11 14:58:43
//  */
// export function tenantInfoDelete (parameter) {
//   return axios({
//     url: '/tenantInfo/delete',
//     method: 'post',
//     data: parameter
//   })
// }
//
// /**
//  * 导出SaaS租户
//  *
//  * @author bwl
//  * @date 2022-08-11 14:58:43
//  */
// export function tenantInfoExport (parameter) {
//   return axios({
//     url: '/tenantInfo/export',
//     method: 'get',
//     params: parameter,
//     responseType: 'blob'
//   })
// }
/**
 * 租户表控制
 *
 * @author b
 * @date 2020/9/4 13:29
 */
import { axios } from '@/utils/request'

/**
 * 租户分页列表
 *
 * @author w
 * @date 2020/9/4 13:31
 */
export function tenantPage (parameter) {
  return axios({
    url: '/tenantInfo/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 增加租户
 *
 * @author l
 * @date 2020/9/4 13:31
 */
export function tenantAdd (parameter) {
  return axios({
    url: '/tenantInfo/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑租户
 *
 * @author b
 * @date 2020/9/4 13:31
 */
export function tenantEdit (parameter) {
  return axios({
    url: '/tenantInfo/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除租户
 *
 * @author w
 * @date 2020/9/4 13:31
 */
export function tenantDelete (parameter) {
  return axios({
    url: '/tenantInfo/delete',
    method: 'post',
    data: parameter
  })
}

/**
 * 查看详情接口
 *
 * @author l
 * @date 2020/9/4 13:31
 */
export function tenantDetail (parameter) {
  return axios({
    url: '/tenantInfo/detail',
    method: 'get',
    params: parameter
  })
}

/**
 * 租户列表
 *
 * @author bwl
 * @date 2020/9/4 13:31
 */
export function tenantListTenants (parameter) {
  return axios({
    url: '/tenantInfo/listTenants',
    method: 'get',
    params: parameter
  })
}
