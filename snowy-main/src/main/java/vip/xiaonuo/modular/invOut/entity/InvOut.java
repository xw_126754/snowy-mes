/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.invOut.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import vip.xiaonuo.core.pojo.base.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.*;
import cn.afterturn.easypoi.excel.annotation.Excel;
import vip.xiaonuo.modular.invdetail.entity.InvDetail;

/**
 * 出库单
 *
 * @author wz
 * @date 2022-06-06 15:55:34
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("dw_inv")
public class InvOut extends BaseEntity {


    /**
     * -1为出库，1为入库
     */
    @Excel(name = "-1为出库，1为入库")
    private Integer category;

    /**
     * 唯一编码
     */
    @Excel(name = "唯一编码")
    private String code;
    /**
     * 主键id
     */
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 入库类型
     */
    @Excel(name = "入库类型")
    private String inType;

    /**
     * 出库类型
     */
    @Excel(name = "出库类型")
    private String outType;

    /**
     * 备注
     */
    @Excel(name = "备注")
    private String remarks;

    /**
     * 出库时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm")
    @Excel(name = "出库时间", databaseFormat = "yyyy-MM-dd HH:mm:ss", format = "yyyy-MM-dd", width = 20)
    private Date time;


    @TableField(exist = false)
    private List<InvDetail> invDetailList;

    /**
     * 仓库id
     */
    @Excel(name = "仓库id")
    private Long warHouId;

}
