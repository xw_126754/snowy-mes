/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.puororder.controller;

import vip.xiaonuo.core.annotion.BusinessLog;
import vip.xiaonuo.core.annotion.Permission;
import vip.xiaonuo.core.enums.LogAnnotionOpTypeEnum;
import vip.xiaonuo.core.pojo.response.ResponseData;
import vip.xiaonuo.core.pojo.response.SuccessResponseData;
import vip.xiaonuo.modular.puororder.param.PuorOrderParam;
import vip.xiaonuo.modular.puororder.service.PuorOrderService;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import javax.annotation.Resource;
import java.util.List;

/**
 * 采购订单控制器
 *
 * @author jiaxin
 * @date 2022-07-27 15:27:15
 */
@RestController
public class PuorOrderController {

    @Resource
    private PuorOrderService puorOrderService;

    /**
     * 查询采购订单
     *
     * @author jiaxin
     * @date 2022-07-27 15:27:15
     */
    @Permission
    @GetMapping("/puorOrder/page")
    @BusinessLog(title = "采购订单_查询", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData page(PuorOrderParam puorOrderParam) {
        return new SuccessResponseData(puorOrderService.page(puorOrderParam));
    }

    /**
     * 添加采购订单
     *
     * @author jiaxin
     * @date 2022-07-27 15:27:15
     */
    @Permission
    @PostMapping("/puorOrder/add")
    @BusinessLog(title = "采购订单_增加", opType = LogAnnotionOpTypeEnum.ADD)
    public ResponseData add(@RequestBody @Validated(PuorOrderParam.add.class) PuorOrderParam puorOrderParam) {
            puorOrderService.add(puorOrderParam);
        return new SuccessResponseData();
    }

    /**
     * 删除采购订单，可批量删除
     *
     * @author jiaxin
     * @date 2022-07-27 15:27:15
     */
    @Permission
    @PostMapping("/puorOrder/delete")
    @BusinessLog(title = "采购订单_删除", opType = LogAnnotionOpTypeEnum.DELETE)
    public ResponseData delete(@RequestBody @Validated(PuorOrderParam.delete.class) List<PuorOrderParam> puorOrderParamList) {
            puorOrderService.delete(puorOrderParamList);
        return new SuccessResponseData();
    }

    /**
     * 编辑采购订单
     *
     * @author jiaxin
     * @date 2022-07-27 15:27:15
     */
    @Permission
    @PostMapping("/puorOrder/edit")
    @BusinessLog(title = "采购订单_编辑", opType = LogAnnotionOpTypeEnum.EDIT)
    public ResponseData edit(@RequestBody @Validated(PuorOrderParam.edit.class) PuorOrderParam puorOrderParam) {
            puorOrderService.edit(puorOrderParam);
        return new SuccessResponseData();
    }

    /**
     * 查看采购订单
     *
     * @author jiaxin
     * @date 2022-07-27 15:27:15
     */
    @Permission
    @GetMapping("/puorOrder/detail")
    @BusinessLog(title = "采购订单_查看", opType = LogAnnotionOpTypeEnum.DETAIL)
    public ResponseData detail(@Validated(PuorOrderParam.detail.class) PuorOrderParam puorOrderParam) {
        return new SuccessResponseData(puorOrderService.detail(puorOrderParam));
    }

    /**
     * 采购订单列表
     *
     * @author jiaxin
     * @date 2022-07-27 15:27:15
     */
    @Permission
    @GetMapping("/puorOrder/list")
    @BusinessLog(title = "采购订单_列表", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData list(PuorOrderParam puorOrderParam) {
        return new SuccessResponseData(puorOrderService.list(puorOrderParam));
    }

    /**
     * 导出系统用户
     *
     * @author jiaxin
     * @date 2022-07-27 15:27:15
     */
    @Permission
    @GetMapping("/puorOrder/export")
    @BusinessLog(title = "采购订单_导出", opType = LogAnnotionOpTypeEnum.EXPORT)
    public void export(PuorOrderParam puorOrderParam) {
        puorOrderService.export(puorOrderParam);
    }

}
