import { axios } from '@/utils/request'

/**
 * 查询装配工单
 *
 * @author wz
 * @date 2022-08-30 13:41:34
 */
export function assPage (parameter) {
  return axios({
    url: '/ass/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 装配工单列表
 *
 * @author wz
 * @date 2022-08-30 13:41:34
 */
export function assList (parameter) {
  return axios({
    url: '/ass/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加装配工单
 *
 * @author wz
 * @date 2022-08-30 13:41:34
 */
export function assAdd (parameter) {
  return axios({
    url: '/ass/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑装配工单
 *
 * @author wz
 * @date 2022-08-30 13:41:34
 */
export function assEdit (parameter) {
  return axios({
    url: '/ass/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除装配工单
 *
 * @author wz
 * @date 2022-08-30 13:41:34
 */
export function assDelete (parameter) {
  return axios({
    url: '/ass/delete',
    method: 'post',
    data: parameter
  })
}

/**
 * 导出装配工单
 *
 * @author wz
 * @date 2022-08-30 13:41:34
 */
export function assExport (parameter) {
  return axios({
    url: '/ass/export',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}
