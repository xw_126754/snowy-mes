package vip.xiaonuo.modular.workorder.enums;

import lombok.Getter;

/**
 * @author 87761
 */
@Getter
public enum WorkOrderTypeEnum {

    /**
     * 顺序工单
     */
    SEQUENTIAL_WORK_ORDER(1,"顺序工单"),
    /**
     * 流程工单
     */
    PROCESS_WORK_ORDER(2,"流程工单"),
    ;



    private final Integer code;

    private final String message;

    WorkOrderTypeEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
